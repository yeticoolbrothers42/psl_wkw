/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgaudin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 13:48:35 by sgaudin           #+#    #+#             */
/*   Updated: 2016/01/31 00:36:28 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H

# include <time.h>
# include <signal.h>
# include <curses.h>
# include "libft/libft.h"
enum
{
	DROITE,
	GAUCHE,
	HAUT,
	BAS
};

enum
{
	WIN_VALUE = 2048
};

void	ft_randominit4(int tab[4][4]);
void	ft_random4(int tab[4][4]);
void	noscreen(void);
void	dep_right4(int tab[4][4]);
void	add_right4(int tab[4][4]);
void	dep_left4(int tab[4][4]);
void	add_left4(int tab[4][4]);
void	dep_up4(int tab[4][4]);
void	add_up4(int tab[4][4]);
void	dep_down4(int tab[4][4]);
void	add_down4(int tab[4][4]);
void	print_grid_ns(int tab[5][5]);
void	dep_right5(int tab[5][5]);
void	add_right5(int tab[5][5]);
void	dep_left5(int tab[5][5]);
void	add_left5(int tab[5][5]);
void	dep_up5(int tab[5][5]);
void	add_up5(int tab[5][5]);
void	dep_down5(int tab[5][5]);
void	add_down5(int tab[5][5]);
void	ft_randominit5(int tab[5][5]);
void	ft_random5(int tab[5][5]);
#endif
