/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_right4.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 19:30:22 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/31 22:39:50 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static int	dep_up4(int tab[4][4])
{
	int t[4];

	ft_memset(t, 0, 20);
	while (t[1] < 4)
	{
		t[0] = 0;
		while (t[0] < 4)
		{
			if (tab[t[0]][t[1]] == 0)
			{
				t[2] = t[0];
				while (tab[t[2]][t[1]] == 0 && t[2] < 4)
					t[2]++;
				if (tab[t[2]][t[1]] != 0 && t[2] < 4)
				{
					tab[t[0]][t[1]] = tab[t[2]][t[1]];
					tab[t[2]][t[1]] = 0;
					t[3] = 1;
				}
			}
			t[0]++;
		}
		t[1]++;
	}
	return (t[3]);
}

static void	add_up42(int tab[4][4], int t[4])
{
	while ((tab[t[0]][t[1]] != tab[t[2]][t[1]]) && t[2] < 4)
	{
		if (tab[t[2]][t[1]] == 0)
			t[2]++;
		else
			break ;
	}
	if (tab[t[0]][t[1]] == tab[t[2]][t[1]] && tab[t[0]][t[1]] != 0 && t[2] < 4)
	{
		tab[t[0]][t[1]] = tab[t[0]][t[1]] + tab[t[2]][t[1]];
		tab[t[2]][t[1]] = 0;
		t[0] = t[2];
		t[3] = 1;
	}
}

int			add_up4(int tab[4][4])
{
	int t[4];

	ft_memset(t, 0, 20);
	while (t[1] < 4)
	{
		t[0] = 0;
		while (t[0] < 3)
		{
			t[2] = t[0] + 1;
			add_up42(tab, t);
			t[0]++;
		}
		t[1]++;
	}
	t[2] = dep_up4(tab);
	if (t[2] == 1 || t[3] == 1)
		return (1);
	return (0);
}
