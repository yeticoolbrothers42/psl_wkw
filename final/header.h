/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgaudin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 13:48:35 by sgaudin           #+#    #+#             */
/*   Updated: 2016/01/31 21:30:08 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H

# include <time.h>
# include <signal.h>
# include <curses.h>
# include "libft/libft.h"
enum
{
	DROITE,
	GAUCHE,
	HAUT,
	BAS
};

enum
{
	WIN_VALUE = 2048
};

void	ft_randominit4(int tab[4][4]);
void	ft_random4(int tab[4][4]);
int		game(void);
int	add_right4(int tab[4][4]);
int	add_left4(int tab[4][4]);
int	add_up4(int tab[4][4]);
int	add_down4(int tab[4][4]);
void	print_grid_ns(int tab[4][4]);
int		affiche(int tab[4][4]);
int		check_zero(int tab[4][4]);
#endif
