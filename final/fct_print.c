/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fct_print.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgaudin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/31 18:01:16 by sgaudin           #+#    #+#             */
/*   Updated: 2016/01/31 19:27:15 by sgaudin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	print_line(int x, int y)
{
	int i;

	i = 2;
	mvprintw(y, 1, "+");
	while (i < x - 1)
	{
		mvprintw(y, i, "-");
		i++;
	}
	mvprintw(y, i, "+");
	refresh();
}

void	print_column(int x, int y)
{
	int i;

	i = 2;
	mvprintw(1, x, "+");
	while (i < (y - 1))
	{
		mvprintw(i, x, "|");
		i++;
	}
	mvprintw(i, x, "+");
	refresh();
}

void	ft_color(int x, int y, int tab[4][4], int i, int j)
{
	y = 0;
	start_color();
	init_pair(1, COLOR_YELLOW, COLOR_RED);
	attron(COLOR_PAIR(1));
	if	(i == 0 && j == 0 && tab[i][j] != 0)
	{
		mvprintw(2, 2, "%*c", (x / 4) - 2, ' ');
	}
	attroff(COLOR_PAIR(1));
}

void	print_nb(int x, int y, int tab[4][4])
{
	int i;/*cood tab i*/
	int j;
	int k;/*cood tab curse visu*/
	int l;

	i = 0;
	k = y / 8 + 1;
	while (i < 4)
	{
		j = 0;
		l = x / 8 + 1;
		while (j < 4)
		{
			if (tab[i][j] != 0)
			{
				mvprintw(k, l, "%d", tab[i][j]);
				ft_color(x, y, tab, i, j);
			}
			j++;
			l = l + (x / 4);
		}
		i++;
		k = k + (y / 4);
	}
}

void	print_all(int x, int y, int tab[4][4])
{
	print_line(x, 1);
	print_line(x, y / 4 + 1);
	print_line(x, y / 2);
	print_line(x, (y / 2 + y / 4));
	print_line(x, y - 1);
	print_column(1, y);
	print_column(x / 2, y);
	print_column(x / 4, y);
	print_column((x / 4) + (x / 2), y);
	print_column(x - 1, y);
	print_nb(x, y, tab);
}

int		affiche(int tab[4][4])
{
	int x;
	int y;

	wclear(stdscr);
	getmaxyx(stdscr, y, x);
	if (x % 2 == 0)
		x -= 2;
	else
		x -= 1;
	if (y % 2 == 0)
		y -= 2;
	else
		y -= 1;
	print_all(x, y, tab);
	return (getch());
}
