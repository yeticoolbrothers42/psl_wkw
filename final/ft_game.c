/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_game.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/31 23:05:09 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/31 23:34:48 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static int		end_game(void)
{
	int c;

	clear();
	mvprintw((getmaxy(stdscr)) / 2, (getmaxx(stdscr)) / 2, "PRESS (ESC) TO QUIT\
			\nOR PRESS (R) TO RETRY");
	refresh();
	c = getch();
	while (c != 'r' && c != 27)
		c = getch();
	if (c == 'r')
	{
		game();
		exit(EXIT_SUCCESS);
	}
	return (0);
}

static int		initncurse(void)
{
	int i;

	i = 2;
	initscr();
	cbreak();
	keypad(stdscr, TRUE);
	curs_set(FALSE);
	while (i <= WIN_VALUE && WIN_VALUE > 0)
	{
		if (i == WIN_VALUE)
			return (1);
		i *= 2;
	}
	clear();
	mvprintw(((getmaxy(stdscr)) / 2), ((getmaxx(stdscr)) / 2), "(WIN_VALUE) IS \
			WRONG\nPLEASE MAKE YOU SURE (WIN_VALUE) IS A POWER OF 2");
	getch();
	endwin();
	return (0);
}

static int		ft_jaime(int tab[4][4], int t[4])
{
	if ((t[3] = (check_zero(tab))) == 1 && t[2] == 1)
	{
		ft_random4(tab);
		return (1);
	}
	return (0);
}

static int		ft_keypad(int tab[4][4], int t[2])
{
	while ((t[0] = affiche(tab)) != 27 && t[2] == 1)
	{
		t[1] = 0;
		if (t[0] == KEY_UP)
			t[1] = add_up4(tab);
		else if (t[0] == KEY_DOWN)
			t[1] = add_down4(tab);
		else if (t[0] == KEY_RIGHT)
			t[1] = add_right4(tab);
		else if (t[0] == KEY_LEFT)
			t[1] = add_left4(tab);
		else if (t[0] == 'r')
		{
			t[2] = 0;
			game();
			exit(EXIT_SUCCESS);
		}
		if ((t[3] = ft_jaime(tab, t) == 0))
		{
			if (end_game() == 0 && t[3] == 0)
				break ;
		}
	}
	return (t[1]);
}

int				game(void)
{
	int tab[4][4];
	int t[4];

	ft_memset(t, 0, 20);
	ft_memset(tab, 0, 4 * 4 * 4);
	ft_randominit4(tab);
	t[2] = 1;
	if (!initncurse())
		return (0);
	t[2] = ft_keypad(tab, t);
	endwin();
	return (0);
}
