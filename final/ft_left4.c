/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_left5.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/31 22:01:16 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/31 22:24:48 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static int	dep_left4(int tab[4][4])
{
	int t[4];

	ft_memset(t, 0, 4);
	while (t[0] < 4)
	{
		t[1] = 0;
		while (t[1] < 4)
		{
			if (tab[t[0]][t[1]] == 0)
			{
				t[2] = t[1];
				while (tab[t[0]][t[2]] == 0 && t[2] < 4)
					t[2]++;
				if (tab[t[0]][t[2]] != 0 && t[2] < 4)
				{
					tab[t[0]][t[1]] = tab[t[0]][t[2]];
					tab[t[0]][t[2]] = 0;
					t[3] = 1;
				}
			}
			t[1]++;
		}
		t[0]++;
	}
	return (t[3]);
}

static void	add_left42(int tab[4][4], int t[4])
{
	while ((tab[t[0]][t[1]] != tab[t[0]][t[2]]) && t[2] < 4)
	{
		if (tab[t[0]][t[2]] == 0)
			t[2]++;
		else
			break ;
	}
	if (tab[t[0]][t[1]] == tab[t[0]][t[2]] && tab[t[0]][t[1]] != 0 && t[2] < 4)
	{
		tab[t[0]][t[1]] = tab[t[0]][t[1]] + tab[t[0]][t[2]];
		tab[t[0]][t[2]] = 0;
		t[1] = t[2];
		t[3] = 1;
	}
}

int			add_left4(int tab[4][4])
{
	int t[4];

	ft_memset(t, 0, 20);
	while (t[0] < 4)
	{
		t[1] = 0;
		while (t[1] < 3)
		{
			t[2] = t[1] + 1;
			add_left42(tab, t);
			t[1]++;
		}
		t[0]++;
	}
	t[2] = dep_left4(tab);
	if (t[2] == 1 || t[3] == 1)
		return (1);
	return (0);
}
