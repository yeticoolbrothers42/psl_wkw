/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_right4.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 19:30:22 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/31 22:22:11 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

static int	dep_right4(int tab[4][4])
{
	int t[4];

	ft_memset(t, 0, 4);
	while (t[0] < 4)
	{
		t[1] = 3;
		while (t[1] > 0)
		{
			if (tab[t[0]][t[1]] == 0)
			{
				t[2] = t[1];
				while (tab[t[0]][t[2]] == 0 && t[2] >= 0)
					t[2]--;
				if (tab[t[0]][t[2]] != 0 && t[2] >= 0)
				{
					tab[t[0]][t[1]] = tab[t[0]][t[2]];
					tab[t[0]][t[2]] = 0;
					t[3] = 1;
				}
			}
			t[1]--;
		}
		t[0]++;
	}
	return (t[3]);
}

static void	add_right42(int tab[4][4], int t[4])
{
	while ((tab[t[0]][t[1]] != tab[t[0]][t[2]]) && t[2] >= 0)
	{
		if (tab[t[0]][t[2]] == 0)
			t[2]--;
		else
			break ;
	}
	if (tab[t[0]][t[1]] == tab[t[0]][t[2]] && tab[t[0]][t[1]] != 0 && t[2] >= 0)
	{
		tab[t[0]][t[1]] = tab[t[0]][t[1]] + tab[t[0]][t[2]];
		tab[t[0]][t[2]] = 0;
		t[1] = t[2];
		t[3] = 1;
	}
}

int			add_right4(int tab[4][4])
{
	int t[4];

	ft_memset(t, 0, 20);
	while (t[0] < 4)
	{
		t[1] = 3;
		while (t[1] > 0)
		{
			t[2] = t[1] - 1;
			add_right42(tab, t);
			t[1]--;
		}
		t[0]++;
	}
	t[2] = dep_right4(tab);
	if (t[2] == 1 || t[3] == 1)
		return (1);
	return (0);
}
