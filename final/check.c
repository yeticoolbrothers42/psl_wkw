/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgaudin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/31 18:45:49 by sgaudin           #+#    #+#             */
/*   Updated: 2016/01/31 23:41:11 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_win(void)
{
	int c;

	clear();
	mvprintw(((getmaxy(stdscr)) / 2), ((getmaxx(stdscr)) / 2), "VICTORY\n PLEAS\
			E PRESS ESCAPE TO QUIT");
	c = getch();
	if (c == 27)
		exit(EXIT_SUCCESS);
}

int		check_endgame(int tab[4][4])
{
	int i;
	int j;

	i = 0;
	while (i < 3)
	{
		j = 0;
		while (j < 3)
		{
			if (tab[i][j] == tab[i][j + 1])
				return (1);
			if (tab[i][j] == tab[i + 1][j])
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int		check_zero(int tab[4][4])
{
	int t[3];

	ft_memset(t, 0, 12);
	while (t[0] < 4)
	{
		t[1] = 0;
		while (t[2] < 4)
		{
			if (tab[t[0]][t[1]] == 0)
				t[2]++;
			else if (tab[t[0]][t[1]] == WIN_VALUE)
				ft_win();
			t[1]++;
		}
		t[0]++;
	}
	if (t[2] > 0)
		return (1);
	else
	{
		if (check_endgame(tab) == 0)
			return (0);
		else
			return (2);
	}
}
