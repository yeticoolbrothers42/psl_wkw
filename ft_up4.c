/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_up4.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 19:49:07 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/30 23:33:31 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void dep_up4(int tab[4][4])
{
	int i;
	int j;
	int k;

	j = 0;
	while (j < 4)
	{
		i = 0;
		while (i < 4)
		{
			if (tab[i][j] == 0)
			{
				k = i;
				while(tab[k][j] == 0 && k < 3)
					k++;
				if (tab[k][j] != 0)
				{
					tab[i][j] = tab[k][j];
					tab[k][j] = 0;
				}
			}
			i++;
		}
		j++;
	}
}

void	add_up4(int tab[4][4])
{
	int i;
	int j;
	int k;

	j = 0;
	while (j < 4)
	{
		i = 3;
		while(i > 0)
		{
			k = i - 1;
			while ((tab[i][j] != tab[k][j]) && k > 0)
			{
				if (tab[k][j] == 0)
					k--;
				else
					break;
			}
			if (tab[i][j] == tab[k][j])
			{
				tab[k][j] = tab[i][j] + tab[k][j];
				tab[i][j] = 0;
			}
			i--;
		}
		j++;
	}
	dep_up4(tab);
}
