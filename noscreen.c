/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   noscreen.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgaudin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 17:16:51 by sgaudin           #+#    #+#             */
/*   Updated: 2016/01/31 00:31:55 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	print_grid_ns(int tab[5][5])
{
	int i, j;

	i = 0;
	while (i < 5)
	{
		j = 0;
		while (j < 5)
		{
			printf("%d ", tab[i][j]);
			j++;
		}
		printf("\n");
		i++;
	}
}

void	noscreen(void)
{
	int tab[5][5];
	int i;

	ft_memset(tab, 0, 5 * 5 * 4);
	ft_randominit5(tab);
	i = 0;
	printf("\n");
	print_grid_ns(tab);
	printf("\n");
	printf("\n VERS LA DROITE\n");
	add_right5(tab);
	ft_random5(tab);
	print_grid_ns(tab);
	printf("\n VERS LE HAUT\n");
	add_up5(tab);
	ft_random5(tab);
	print_grid_ns(tab);
	printf("\n VERS LE BAS\n");
	add_down5(tab);
	ft_random5(tab);
	print_grid_ns(tab);
	printf("\n VERS LA GAUCHE\n");
	add_left5(tab);
	ft_random5(tab);
	print_grid_ns(tab);
	printf("\nFINAL JUSTE AU DESSUS\n");
}
