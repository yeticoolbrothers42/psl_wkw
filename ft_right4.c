/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_right4.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 19:30:22 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/30 19:34:12 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


void dep_right4(int tab[4][4])
{
	int i;
	int j;
	int k;

	i = 0;
	while (i < 4)
	{
		k = 0;
		j = 3;
		while (j > 0)
		{
			if (tab[i][j] == 0)
			{
				k = j;
				while(tab[i][k] == 0 && k > 0)
					k--;
				if (tab[i][k] != 0)
				{
					tab[i][j] = tab[i][k];
					tab[i][k] = 0;
				}
			}
			j--;
		}
		i++;
	}
}
void	add_right4(int tab[4][4])
{
	int i;
	int j;
	int k;
	i = 0;
	while (i < 4)
	{
		j = 0;
		while(j < 3)
		{
			k = j + 1;
			while ((tab[i][j] != tab[i][k]))
			{
				if (tab[i][k] == 0)
					k++;
				else
					break;
			}
			if (tab[i][j] == tab[i][k])
			{
				tab[i][k] = tab[i][j] + tab[i][k];
				tab[i][j] = 0;
			}
			j++;
		}
		i++;
	}
	dep_right4(tab);
}
