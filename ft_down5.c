/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_down4.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 20:02:18 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/31 00:15:21 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void dep_down5(int tab[5][5])
{
	int i;
	int j;
	int k;

	j = 0;
	while (j < 5)
	{
		i = 4;
		while (i > 0)
		{
			if (tab[i][j] == 0)
			{
				k = i;
				while(tab[k][j] == 0 && k > 0)
					k--;
				if (tab[k][j] != 0)
				{
					tab[i][j] = tab[k][j];
					tab[k][j] = 0;
				}
			}
			i--;
		}
		j++;
	}
}

void	add_down5(int tab[5][5])
{
	int i;
	int j;
	int k;

	j = 0;
	while (j < 5)
	{
		i = 0;
		while(i < 4)
		{
			k = i + 1;
			while ((tab[i][j] != tab[k][j]))
			{
				if (tab[k][j] == 0)
					k++;
				else
					break;
			}
			if (tab[i][j] == tab[k][j])
			{
				tab[k][j] = tab[i][j] + tab[k][j];
				tab[i][j] = 0;
			}
			i++;
		}
		j++;
	}
	dep_down5(tab);
}
