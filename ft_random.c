/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_random.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 14:36:30 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/30 19:26:14 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_randominit(int tab[4][4])
{
	int t[2];
	int i;

	srand(time(NULL));
	i = 0;
	t[0] = 0;
	t[1] = 0;
	while (i < 2)
	{
		t[0] = (rand() % (4));
		t[1] = (rand() % (4));
		if (tab[t[0]][t[1]] == 0)
		{
			while (tab[t[0]][t[1]] != 2 && tab[t[0]][t[1]] != 4)
				tab[t[0]][t[1]] = (rand() % (4 - 2) + 2);
		}
		else
			i--;
		i++;
	}
}

void	ft_random(int tab[4][4])
{
	int t[2];

	srand(time(NULL));
	t[0] = 0;
	t[1] = 0;
	while (1)
	{
		t[0] = (rand() % 4);
		t[1] = (rand() % 4);
		if (tab[t[0]][t[1]] == 0)
			break ;
	}
	while (tab[t[0]][t[1]] != 2 && tab[t[0]][t[1]] != 4)
		tab[t[0]][t[1]] = (rand() % 5);
}
