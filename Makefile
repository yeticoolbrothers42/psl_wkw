#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sgaudin <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/30 15:35:21 by sgaudin           #+#    #+#              #
#    Updated: 2016/01/31 00:38:37 by pbenoit          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = game_2048

SRC = wkw.c ft_random4.c noscreen.c ft_right4.c ft_left4.c ft_up4.c ft_down4.c\
	  ft_right5.c ft_left5.c ft_up5.c ft_down5.c ft_random5.c

HEADER = header.h

CC = gcc -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	$(CC) $(SRC) -L./libft/ -lft -lncurses -o $(NAME)

clean:
	rm -rf *~

fclean: clean
	rm -rf $(NAME)

re: fclean all

push:
	git commit -am "make push"
	git push
