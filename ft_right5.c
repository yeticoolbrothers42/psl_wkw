/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_right4.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 19:30:22 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/31 00:17:42 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


void dep_right5(int tab[5][5])
{
	int i;
	int j;
	int k;

	i = 0;
	while (i < 5)
	{
		k = 0;
		j = 4;
		while (j > 0)
		{
			if (tab[i][j] == 0)
			{
				k = j;
				while(tab[i][k] == 0 && k > 0)
					k--;
				if (tab[i][k] != 0)
				{
					tab[i][j] = tab[i][k];
					tab[i][k] = 0;
				}
			}
			j--;
		}
		i++;
	}
}
void	add_right5(int tab[5][5])
{
	int i;
	int j;
	int k;
	i = 0;
	while (i < 5)
	{
		j = 0;
		while(j < 4)
		{
			k = j + 1;
			while ((tab[i][j] != tab[i][k]))
			{
				if (tab[i][k] == 0)
					k++;
				else
					break;
			}
			if (tab[i][j] == tab[i][k])
			{
				tab[i][k] = tab[i][j] + tab[i][k];
				tab[i][j] = 0;
			}
			j++;
		}
		i++;
	}
	dep_right5(tab);
}
