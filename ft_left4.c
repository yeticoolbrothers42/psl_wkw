/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_left4.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pbenoit <pbenoit@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 19:49:21 by pbenoit           #+#    #+#             */
/*   Updated: 2016/01/30 23:46:29 by pbenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void dep_left4(int tab[4][4])
{
	int i;
	int j;
	int k;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if (tab[i][j] == 0)
			{
				k = j;
				while(tab[i][k] == 0 && k < 4)
					k++;
				if (tab[i][k] != 0)
				{
					tab[i][j] = tab[i][k];
					tab[i][k] = 0;
				}
			}
			j++;
		}
		i++;
	}
}

void	add_left4(int tab[4][4])
{
	int i;
	int j;
	int k;

	i = 0;
	while (i < 4)
	{
		j = 3;
		while(j > 1)
		{
			k = j - 1;
			while ((tab[i][j] != tab[i][k]) && k > 0)
			{
				if (tab[i][k] == 0)
					k--;
				else
					break;
			}
			if (tab[i][j] == tab[i][k])
			{
				tab[i][k] = tab[i][j] + tab[i][k];
				tab[i][j] = 0;
			}
			j--;
		}
		i++;
	}
	dep_left4(tab);
}
